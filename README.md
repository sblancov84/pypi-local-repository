# Description

Proof of Concept to create a local pypi repository following
[PEP 503](https://www.python.org/dev/peps/pep-0503/).

# Requirements

* Docker
* docker-compose

# Run

Just type the next:

    docker-compose up

Now, there is a new service running in the 8000 port on your localhost. It is
a simple http server which provide an access point for your new pypi repository.

## How to download packages

To keep clean your system environment we go to create a new virtualenv. I
usually use pyenv for this so:

    pyenv virtualenv 3.7.3 local-pypi
    pyenv activate local-pypi

Then, you can use pip to download all the packages in that repository, there is
only one package inside because I have put only the "parse" package. A special
flag has to be used for pip. Ok, let's go:

    pip install --index-url parse

Great! now you can see your parse version is 1.12.1 and the last version in the
official repositories is >=1.15.0 (1.15.0 is the last released today).

## How to upload packages

To upload packages to the repository, just create a tar.gz of your package, then
create a new directory into repo and then copy tar.gz into the new directory as
I have done with parse.
