FROM python:slim

WORKDIR /repo

COPY repo /repo
COPY entrypoint.sh /scripts/entrypoint.sh

RUN chmod +x /scripts/entrypoint.sh

EXPOSE 8000

ENTRYPOINT [ "/scripts/entrypoint.sh" ]
